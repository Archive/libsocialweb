# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-05-05 19:28+0000\n"
"PO-Revision-Date: 2011-07-28 10:50+0300\n"
"Last-Translator: Yuri Myasoedov <omerta13@yandex.ru>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-Language: Russian\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../services/facebook/facebook.keys.in.h:1
msgid "Facebook"
msgstr "Facebook"

#: ../services/facebook/facebook.keys.in.h:2
msgid "Facebook helps you connect and share with the people in your life."
msgstr "Facebook помогает вам общаться и делиться с людьми."

#: ../services/flickr/flickr.keys.in.h:1
msgid "Flickr"
msgstr "Flickr"

#: ../services/flickr/flickr.keys.in.h:2
msgid "Flickr - almost certainly the best online photo management and sharing application in the world."
msgstr "Flickr — наверное, самое лучшее онлайн-приложение в мире для работы с фотографиями и обмена ими."

#. Translators "[track title] by [artist]"
#: ../services/lastfm/lastfm-item-view.c:361
#, c-format
msgid "%s by %s"
msgstr "%s в исполнении %s"

#: ../services/lastfm/lastfm-item-view.c:365
msgid "Unknown"
msgstr "Неизвестно"

#: ../services/lastfm/lastfm.keys.in.h:1
msgid "Last.fm"
msgstr "Last.fm"

#: ../services/lastfm/lastfm.keys.in.h:2
msgid "Last.fm is a music service that learns what you love. Every track you play will tell your Last.fm profile something about what you like. It can connect you to other people who like what you like - and recommend songs from their music collections and yours too."
msgstr "Last.fm — это музыкальный сервис, который изучает ваши вкусы. Каждый трек, выбранный вами, предоставляет информацию в профиль Last.fm о том, что вам нравится. Этот сервис может связать вас с другими пользователями, которые разделяют ваши интересы, - и порекомендовать песни из их музыкальных коллекций, а также из вашей."

#: ../services/smugmug/smugmug.keys.in.h:1
msgid "SmugMug"
msgstr "SmugMug"

#: ../services/smugmug/smugmug.keys.in.h:2
msgid "Your Photos Look Better Here. Billions of happy photos. Millions of passionate customers."
msgstr "Лучшее место для ваших снимков. Миллиарды отличных снимков. Миллионы страстных клиентов."

#. This format is for tweets announcing twitpic URLs, "[tweet] [url]".
#: ../services/twitter/twitter.c:829
#, c-format
#| msgid "%s by %s"
msgid "%s %s"
msgstr "%s %s"

#: ../services/twitter/twitter.keys.in.h:1
msgid "Twitter"
msgstr "Twitter"

#: ../services/twitter/twitter.keys.in.h:2
msgid "Twitter is a service for friends, family, and co–workers to communicate and stay connected through the exchange of quick, frequent answers to one simple question: What are you doing?"
msgstr "Twitter — это сервис для друзей, родственников и коллег по работе, который помогает им оставаться на связи и общаться, обмениваясь быстрыми и частыми ответами на простой вопрос: Что ты делаешь?"

#: ../services/vimeo/vimeo.keys.in.h:1
msgid "From the beginning, Vimeo was created by filmmakers and video creators who wanted to share their creative work, along with intimate personal moments of their everyday life. As time went on, like-minded people came to the site and built a community of positive, encouraging individuals with a wide range of video interests. We hope that you feel inspired to show us both your creative side as well as your friendly side."
msgstr "Первоначально Vimeo создавался для энтузиастов, снимающих своё видео, которые хотят поделиться своим творчеством, а также личными моментами из своей повседневной жизни. Шло время, на сайт пришли люди со схожими взглядами и построили позитивное сообщество, которое вдохновило большое количество участников, увлечённых видео. Мы надеемся, что вы также поделитесь своим творчеством."

#: ../services/vimeo/vimeo.keys.in.h:2
msgid "Vimeo"
msgstr "Vimeo"

#: ../services/photobucket/photobucket.keys.in.h:1
msgid "Photobucket"
msgstr "Photobucket"

#: ../services/photobucket/photobucket.keys.in.h:2
msgid "Snap and show it off! Photobucket makes it easy to share your photos and videos anywhere."
msgstr "Сними и покажи! С помощью Photobucket очень просто делиться снимками и видео."

#: ../services/plurk/plurk.keys.in.h:1
msgid "Plurk"
msgstr "Plurk"

#: ../services/plurk/plurk.keys.in.h:2
msgid "Plurk is a web site that allows people to share ideas."
msgstr "Plurk — это веб-сайт, который позволяет делиться друг с другом идеями."

#: ../services/sina/sina.keys.in.h:1
msgid "Sina"
msgstr "Sina"

#: ../services/sina/sina.keys.in.h:2
msgid "Sina microblogging"
msgstr "Служба микроблогинга Sina"

#: ../services/youtube/youtube.keys.in.h:1
msgid "YouTube"
msgstr "YouTube"

#: ../services/youtube/youtube.keys.in.h:2
msgid "Youtube is a popular video sharing website"
msgstr "Youtube — популярный сервис для публикации видео"

#: ../services/myspace/myspace.keys.in.h:1
msgid "MySpace"
msgstr "MySpace"

#: ../services/myspace/myspace.keys.in.h:2
msgid "MySpace is a leading social portal for connecting people, content, and culture from around the world. Create a profile, share photos, professional and viral videos, blog, instant message your friends, and listen to music on the world’s largest music community."
msgstr "MySpace — ведущий социальный портал, на котором предоставлена возможность общения между людьми, обмена различными файлами и информацией о культурных особенностях во всем мире. Создайте свой профиль, поделитесь фотографиями, профессиональными и любительскими видеороликами, ведите свой блог, обменивайтесь быстрыми сообщениями с друзьями и слушайте музыку в самом большом мировом сообществе."

