# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# Aviary.pl
# Jeśli masz jakiekolwiek uwagi odnoszące się do tłumaczenia lub chcesz
# pomóc w jego rozwijaniu i pielęgnowaniu, napisz do nas:
# gnomepl@aviary.pl
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# Andrzej Zaborowski <andrew.zaborowski@intel.com>, 2009.
# mekaminX <mariax.e.kaminski@intel.com>, 2009.
# Piotr Drąg <piotrdrag@gmail.com>, 2011-2012.
# Aviary.pl <gnomepl@aviary.pl>, 2011-2012.
msgid ""
msgstr ""
"Project-Id-Version: libsocialweb\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-09-04 18:58+0200\n"
"PO-Revision-Date: 2012-09-04 19:03+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <gnomepl@aviary.pl>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Poedit-Language: Polish\n"
"X-Poedit-Country: Poland\n"

#: ../services/facebook/facebook.keys.in.h:1
msgid "Facebook"
msgstr "Facebook"

#: ../services/facebook/facebook.keys.in.h:2
msgid "Facebook helps you connect and share with the people in your life."
msgstr ""
"Facebook pomaga utrzymywać kontakt i dzielić się rzeczami z innymi osobami."

#: ../services/flickr/flickr.keys.in.h:1
msgid "Flickr"
msgstr "Flickr"

#: ../services/flickr/flickr.keys.in.h:2
msgid ""
"Flickr - almost certainly the best online photo management and sharing "
"application in the world."
msgstr ""
"Flickr - prawdopodobnie najlepsza sieciowa platforma do zarządzania i "
"dzielenia się zdjęciami."

#. Translators "[track title] by [artist]"
#: ../services/lastfm/lastfm-item-view.c:361
#, c-format
msgid "%s by %s"
msgstr "%s w wykonaniu %s"

#: ../services/lastfm/lastfm-item-view.c:365
msgid "Unknown"
msgstr "Nieznany"

# <it's a proper name, not translatable>
#: ../services/lastfm/lastfm.keys.in.h:1
msgid "Last.fm"
msgstr "Last.fm"

#: ../services/lastfm/lastfm.keys.in.h:2
msgid ""
"Last.fm is a music service that learns what you love. Every track you play "
"will tell your Last.fm profile something about what you like. It can connect "
"you to other people who like what you like - and recommend songs from their "
"music collections and yours too."
msgstr ""
"Last.fm to serwis muzyczny, który uczy się czego lubisz słuchać. Każdy "
"odegrany utwór wzbogaca wiedzę profilu Last.fm o preferencjach użytkownika. "
"Może wtedy łączyć z osobami, które lubią to samo lub polecać utwory z "
"kolekcji."

#: ../services/smugmug/smugmug.keys.in.h:1
msgid "SmugMug"
msgstr "SmugMug"

#: ../services/smugmug/smugmug.keys.in.h:2
msgid ""
"Your Photos Look Better Here. Billions of happy photos. Millions of "
"passionate customers."
msgstr ""
"Tutaj zdjęcia wyglądają lepiej. Miliardy zadowolonych zdjęć. Miliony "
"klientów z pasją."

#. This format is for tweets announcing twitpic URLs, "[tweet] [url]".
#: ../services/twitter/twitter.c:830
#, c-format
msgid "%s %s"
msgstr "%s %s"

#: ../services/twitter/twitter.keys.in.h:1
msgid "Twitter"
msgstr "Twitter"

#: ../services/twitter/twitter.keys.in.h:2
msgid ""
"Twitter is a service for friends, family, and co–workers to communicate and "
"stay connected through the exchange of quick, frequent answers to one simple "
"question: What are you doing?"
msgstr ""
"Twitter to serwis do komunikacji z przyjaciółmi, rodziną i znajomymi z pracy "
"za pomocą częstego (i zwięzłego) odpowiadania na jedno proste pytanie: co "
"robisz?"

#: ../services/vimeo/vimeo.keys.in.h:1
msgid "Vimeo"
msgstr "Vimeo"

# no need to be so chatty
#: ../services/vimeo/vimeo.keys.in.h:2
msgid ""
"From the beginning, Vimeo was created by filmmakers and video creators who "
"wanted to share their creative work, along with intimate personal moments of "
"their everyday life. As time went on, like-minded people came to the site "
"and built a community of positive, encouraging individuals with a wide range "
"of video interests. We hope that you feel inspired to show us both your "
"creative side as well as your friendly side."
msgstr ""
"Serwis Vimeo został utworzony przez twórców filmów, którzy chcieli dzielić "
"się swoją pracą razem z chwilami ze swojego codziennego życia."

#: ../services/photobucket/photobucket.keys.in.h:1
msgid "Photobucket"
msgstr "Photobucket"

#: ../services/photobucket/photobucket.keys.in.h:2
msgid ""
"Snap and show it off! Photobucket makes it easy to share your photos and "
"videos anywhere."
msgstr ""
"Serwis Photobucket ułatwia dzielenie się zdjęciami i nagraniami wideo w "
"każdym miejscu."

#: ../services/plurk/plurk.keys.in.h:1
msgid "Plurk"
msgstr "Plurk"

#: ../services/plurk/plurk.keys.in.h:2
msgid "Plurk is a web site that allows people to share ideas."
msgstr "Plurk to serwis umożliwiający dzielenie się pomysłami."

#: ../services/sina/sina.keys.in.h:1
msgid "Sina"
msgstr "Sina"

#: ../services/sina/sina.keys.in.h:2
msgid "Sina microblogging"
msgstr "Mikroblogi Sina"

#: ../services/youtube/youtube.keys.in.h:1
msgid "YouTube"
msgstr "YouTube"

#: ../services/youtube/youtube.keys.in.h:2
msgid "Youtube is a popular video sharing website"
msgstr "YouTube to popularny serwis do dzielenia się nagraniami wideo"

#: ../services/myspace/myspace.keys.in.h:1
msgid "MySpace"
msgstr "MySpace"

# no need to be so chatty
#: ../services/myspace/myspace.keys.in.h:2
msgid ""
"MySpace is a leading social portal for connecting people, content, and "
"culture from around the world. Create a profile, share photos, professional "
"and viral videos, blog, instant message your friends, and listen to music on "
"the world’s largest music community."
msgstr ""
"MySpace jest wiodącym serwisem społecznościowym, łączącym ludzi, treści i "
"kulturę z całego świata."
